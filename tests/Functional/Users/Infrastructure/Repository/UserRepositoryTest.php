<?php

declare(strict_types=1);

namespace App\Tests\Functional\Users\Infrastructure\Repository;

use App\Users\Domain\Factory\UserFactory;
use App\Users\Infrastructure\Repository\UserRepository;
use Faker\Factory;
use Faker\Generator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserRepositoryTest extends WebTestCase
{
    private UserRepository $repository;
    private Generator $faker;
    private UserFactory $userFactory;

    /**
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->repository = static::getContainer()->get(UserRepository::class);
        $this->userFactory = static::getContainer()->get(UserFactory::class);
        $this->faker = Factory::create();
    }
    public function test_user_added_successful():void
    {
        $email = $this->faker->email();
        $password = $this->faker->password();

        $user = $this->userFactory->create($email, $password);

        $this->repository->add($user);

        $existingUser = $this->repository->findByUlid($user->getId());
        $this->assertEquals($user->getId(), $existingUser->getId());
    }
}
