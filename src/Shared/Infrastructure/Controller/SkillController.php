<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Controller;

use App\Exercise\Application\Dto\SkillCreatedDto;
use App\Exercise\Application\Dto\SkillCreateDto;
use App\Exercise\Application\Manager\SkillManager;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: 'api/v1/skills')]
class SkillController extends AbstractFOSRestController
{
    public function __construct(
        private readonly SkillManager $skillManager,
    )
    {
    }

    #[OA\RequestBody(
        required: true,
        content: new OA\JsonContent(ref: new Model(type: SkillCreateDto::class))
    )]
    #[OA\Response(
        response: Response::HTTP_CREATED,
        description: 'Return skill',
        content: new OA\JsonContent(
            allOf: [
                new OA\Schema(ref: new Model(type: SkillCreatedDto::class))
            ]
        )
    )]
    #[OA\Response(
        response: Response::HTTP_BAD_REQUEST,
        description: 'Bad request',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 400
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                    example: 'Название навыка обязательное поле'
                ),
            ],
        )
    )]
    #[OA\Tag(name: 'Skill')]
    #[Route('', name: 'storeSkill', methods: ['POST'])]
    #[ParamConverter('dto', converter: 'fos_rest.request_body')]
    public function storeSkill(SkillCreateDto $dto): Response
    {
        $view = $this->view($this->skillManager->create($dto), Response::HTTP_CREATED);

        return $this->handleView($view);
    }

    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Return skill',
        content: new OA\JsonContent(
            allOf: [
                new OA\Schema(ref: new Model(type: SkillCreatedDto::class))
            ]
        )
    )]
    #[OA\Response(
        response: Response::HTTP_NOT_FOUND,
        description: 'Not found',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 404
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                    example: 'Не удалось найти навык по идентификатору = 01HFXHHN6M7H1J5D0X3ZX5M13M'
                ),
            ],
        )
    )]
    #[OA\Tag(name: 'Skill')]
    #[Route('/{skill}', name: 'showSkill', methods: ['GET'])]
    public function showSkill(string $skill): Response
    {
        $view = $this->view($this->skillManager->findSkillById($skill), Response::HTTP_OK);

        return $this->handleView($view);
    }

    #[OA\RequestBody(
        required: true,
        content: new OA\JsonContent(ref: new Model(type: SkillCreateDto::class))
    )]
    #[OA\Response(
        response: Response::HTTP_CREATED,
        description: 'Return skill',
        content: new OA\JsonContent(
            allOf: [
                new OA\Schema(ref: new Model(type: SkillCreatedDto::class))
            ]
        )
    )]
    #[OA\Response(
        response: Response::HTTP_BAD_REQUEST,
        description: 'Bad request',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 400
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                    example: 'Название навыка обязательное поле'
                ),
            ],
        )
    )]
    #[OA\Tag(name: 'Skill')]
    #[Route('/{skill}', name: 'updateSkill', methods: ['PUT'])]
    #[ParamConverter('dto', converter: 'fos_rest.request_body')]
    public function updateSkill(SkillCreateDto $dto, string $skill): Response
    {
        $view = $this->view($this->skillManager->update($dto, $skill), Response::HTTP_OK);

        return $this->handleView($view);
    }

    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Return skill',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'data',
                    type: 'array',
                    items: new OA\Items(ref: new Model(type: SkillCreatedDto::class))
                ),]

        )
    )]
    #[OA\Tag(name: 'Skill')]
    #[Route('', name: 'indexSkill', methods: ['GET'])]
    public function indexSkill(): Response
    {
        $view = $this->view($this->skillManager->findAll(), Response::HTTP_OK);

        return $this->handleView($view);
    }

    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Was deleted',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'success',
                    type: 'boolean',
                    example: true
                ),
            ]
        )
    )]
    #[OA\Response(
        response: Response::HTTP_NOT_FOUND,
        description: 'Not found',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 404
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                    example: 'Не удалось найти навык по идентификатору = 01HFXHHN6M7H1J5D0X3ZX5M13M'
                ),
            ],
        )
    )]
    #[OA\Tag(name: 'Skill')]
    #[Route('/{skill}', name: 'destroySkill', methods: ['DELETE'])]
    public function destroySkill(string $skill): Response
    {
        $result = $this->skillManager->destroy($skill);
        $view = $this->view(['success' => $result], Response::HTTP_OK);

        return $this->handleView($view);
    }
}