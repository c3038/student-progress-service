<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Controller;

use App\Users\Application\Dto\UserCreatedDto;
use App\Users\Application\Dto\UserCreateDto;
use App\Users\Application\Manager\UserManager;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: 'api/v1')]
class UserController extends AbstractFOSRestController
{
    public function __construct(
        private readonly UserManager $userManager,
    )
    {
    }

    #[OA\RequestBody(
        required: true,
        content: new OA\JsonContent(ref: new Model(type: UserCreateDto::class))
    )]
    #[OA\Response(
        response: Response::HTTP_CREATED,
        description: 'Return user',
        content: new OA\JsonContent(
            allOf: [
                new OA\Schema(ref: new Model(type: UserCreatedDto::class))
            ]
        )
    )]
    #[OA\Response(
        response: Response::HTTP_BAD_REQUEST,
        description: 'Bad request',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 400
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                    example: 'Пароль и email обязательны'
                ),
            ],
        )
    )]
    #[OA\Tag(name: 'User')]
    #[Route('/users', name: 'store', methods: ['POST'])]
    #[ParamConverter('dto', converter: 'fos_rest.request_body')]
    public function store(UserCreateDto $dto): Response
    {
        $view = $this->view($this->userManager->create($dto), Response::HTTP_CREATED);

        return $this->handleView($view);
    }
}