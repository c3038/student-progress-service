<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Controller;

use App\Exercise\Application\Dto\ExerciseCreateDto;
use App\Exercise\Application\Dto\ExerciseDto;
use App\Exercise\Application\Manager\ExerciseManager;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: 'api/v1/exercises')]
class ExerciseController extends AbstractFOSRestController
{
    public function __construct(
        private readonly ExerciseManager $exerciseManager,
    )
    {
    }

    #[OA\RequestBody(
        required: true,
        content: new OA\JsonContent(ref: new Model(type: ExerciseCreateDto::class))
    )]
    #[OA\Response(
        response: Response::HTTP_CREATED,
        description: 'Return exercise',
        content: new OA\JsonContent(
            allOf: [
                new OA\Schema(ref: new Model(type: ExerciseDto::class))
            ]
        )
    )]
    #[OA\Response(
        response: Response::HTTP_BAD_REQUEST,
        description: 'Bad request',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 400
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                    example: 'Название урока обязательное поле'
                ),
            ],
        )
    )]
    #[OA\Tag(name: 'Exercise')]
    #[Route('', name: 'storeExercises', methods: ['POST'])]
    #[ParamConverter('dto', class: ExerciseCreateDto::class, converter: 'fos_rest.request_body')]
    public function storeExercises(ExerciseCreateDto $dto): Response
    {
        $view = $this->view($this->exerciseManager->create($dto), Response::HTTP_CREATED);

        return $this->handleView($view);
    }

    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Return exercise',
        content: new OA\JsonContent(
            allOf: [
                new OA\Schema(ref: new Model(type: ExerciseDto::class))
            ]
        )
    )]
    #[OA\Response(
        response: Response::HTTP_NOT_FOUND,
        description: 'Not found',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 404
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                    example: 'Не удалось найти задание по идентификатору = 01HFXHHN6M7H1J5D0X3ZX5M13M'
                ),
            ],
        )
    )]
    #[OA\Tag(name: 'Exercise')]
    #[Route('/{exercise}', name: 'showExercise', methods: ['GET'])]
    public function showExercises(string $exercise): Response
    {
        $view = $this->view($this->exerciseManager->findExerciseById($exercise), Response::HTTP_OK);

        return $this->handleView($view);
    }

    #[OA\RequestBody(
        required: true,
        content: new OA\JsonContent(ref: new Model(type: ExerciseCreateDto::class))
    )]
    #[OA\Response(
        response: Response::HTTP_CREATED,
        description: 'Return exercise',
        content: new OA\JsonContent(
            allOf: [
                new OA\Schema(ref: new Model(type: ExerciseDto::class))
            ]
        )
    )]
    #[OA\Response(
        response: Response::HTTP_BAD_REQUEST,
        description: 'Bad request',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 400
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                    example: 'Название урока обязательное поле'
                ),
            ],
        )
    )]
    #[OA\Tag(name: 'Exercise')]
    #[Route('/{exercise}', name: 'updateExercise', methods: ['PUT'])]
    #[ParamConverter('dto', converter: 'fos_rest.request_body')]
    public function updateExercises(ExerciseCreateDto $dto, string $exercise): Response
    {
        $view = $this->view($this->exerciseManager->update($dto, $exercise), Response::HTTP_OK);

        return $this->handleView($view);
    }

    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Return exercise',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'data',
                    type: 'array',
                    items: new OA\Items(ref: new Model(type: ExerciseDto::class))
                ),]

        )
    )]
    #[OA\Tag(name: 'Exercise')]
    #[Route('', name: 'indexExercise', methods: ['GET'])]
    public function indexSkill(): Response
    {
        $view = $this->view($this->exerciseManager->findAll(), Response::HTTP_OK);

        return $this->handleView($view);
    }

    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Was deleted',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'success',
                    type: 'boolean',
                    example: true
                ),
            ]
        )
    )]
    #[OA\Response(
        response: Response::HTTP_NOT_FOUND,
        description: 'Not found',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 404
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                    example: 'Не удалось найти задание по идентификатору = 01HFXHHN6M7H1J5D0X3ZX5M13M'
                ),
            ],
        )
    )]
    #[OA\Tag(name: 'Exercise')]
    #[Route('/{exercise}', name: 'destroyExercise', methods: ['DELETE'])]
    public function destroySkill(string $exercise): Response
    {
        $result = $this->exerciseManager->destroy($exercise);
        $view = $this->view(['success' => $result], Response::HTTP_OK);

        return $this->handleView($view);
    }
}