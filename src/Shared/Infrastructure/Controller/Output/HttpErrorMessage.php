<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Controller\Output;

final class HttpErrorMessage
{
    public function __construct(
        public int $code, public string $message)
    {
    }
}
