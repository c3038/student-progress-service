<?php

declare(strict_types=1);

namespace App\Shared\Domain\Manager;

use Symfony\Component\Uid\Ulid;

class UlidManger
{
    public static function generate(): string
    {
        return Ulid::generate();
    }

    public static function toHexadecimalUlid(string $strUlid): string
    {
        return  bin2hex(Ulid::fromString($strUlid)->toBinary());
    }
}