<?php

declare(strict_types=1);

namespace App\Shared\Application\EventListener;

use App\Shared\Infrastructure\Controller\Output\HttpErrorMessage;
use Doctrine\DBAL\Types\ConversionException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use App\Shared\Domain\Exception\EntityNotFoundException;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class EntityNotFoundListener
{
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if ($exception instanceof EntityNotFoundException) {
            $response = new JsonResponse(
                new HttpErrorMessage(Response::HTTP_NOT_FOUND, $exception->getMessage()),
                Response::HTTP_NOT_FOUND
            );
            $event->setResponse($response);
        }

        if ($exception instanceof ConversionException) {
            $response = new JsonResponse(
                new HttpErrorMessage(Response::HTTP_NOT_FOUND, 'Переданы не верные данные'),
                Response::HTTP_NOT_FOUND
            );
            $event->setResponse($response);
        }
    }
}