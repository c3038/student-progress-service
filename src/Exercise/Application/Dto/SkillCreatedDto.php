<?php

declare(strict_types=1);

namespace App\Exercise\Application\Dto;

use OpenApi\Attributes as OA;

#[OA\Schema(
    schema: 'SkillCreatedDto',
    title: 'Объект ответа для создания/редактирования навыка',
    properties: [
        new OA\Property(
            property: 'id',
            type: 'string',
            example: '01HFNSPQV1MJ8N5KCWF625E8Q4'
        ),
        new OA\Property(
            property: 'title',
            type: 'string',
            example: 'skill 1'
        ),
        new OA\Property(
            property: 'description',
            type: 'string',
            example: 'some description'
        ),
        new OA\Property(
            property: 'created_at',
            type: 'string',
            example: '23-11-2023 07:38:05'
        ),
        new OA\Property(
            property: 'updated_at',
            type: 'string',
            example: '23-11-2023 07:47:48'
        ),
    ]
)]
final class SkillCreatedDto
{
    public function __construct(
        private readonly string $id,
        private readonly string $title,
        private readonly string $description,
        private readonly string $createdAt,
        private readonly string $updatedAt
    )
    {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

}