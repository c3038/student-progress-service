<?php

declare(strict_types=1);

namespace App\Exercise\Application\Dto;

use Doctrine\Common\Collections\Collection;
use OpenApi\Attributes as OA;

#[OA\Schema(
    schema: 'ExerciseDto',
    title: 'Объект ответа для создания/редактирования задания',
    properties: [
        new OA\Property(
            property: 'skills',
            type: 'array',
            items: new OA\Items(
                properties: [
                    new OA\Property(
                        property: 'title',
                        type: 'string',
                        example: 'exercise 4'
                    ),
                    new OA\Property(
                        property: 'description',
                        type: 'string',
                        example: 'some description'
                    ),
                    new OA\Property(
                        property: 'amount',
                        type: 'integer',
                        example: 12
                    ),
                ]
            )
        ),
        new OA\Property(
            property: 'id',
            type: 'string',
            example: '01HFXHTMZBD30K3123QY1YH42N'
        ),
        new OA\Property(
            property: 'title',
            type: 'string',
            example: 'exercise 4'
        ),
        new OA\Property(
            property: 'description',
            type: 'string',
            example: 'some description'
        ),
        new OA\Property(
            property: 'created_at',
            type: 'string',
            example: '23-11-2023 07:38:05'
        ),
        new OA\Property(
            property: 'updated_at',
            type: 'string',
            example: '23-11-2023 07:47:48'
        ),
    ]
)]
final class ExerciseDto
{
    /** @param SWADto[] $skills */
    private readonly iterable $skills;

    public function __construct(
        private readonly string $id,
        private readonly string $title,
        private readonly string $description,
        /** @param Collection $skills */
        Collection              $skills,
        private readonly string $createdAt,
        private readonly string $updatedAt
    )
    {
        $this->skills = $skills->map(static fn($data) => new SWADto(
            $data->getSkill()->getTitle(),
            $data->getSkill()->getDescription(),
            $data->getAmount()
        ))->getValues();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;

    }

    public function getSkills(): iterable
    {
        return $this->skills;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }
}