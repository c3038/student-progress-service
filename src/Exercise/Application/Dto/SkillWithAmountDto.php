<?php

declare(strict_types=1);

namespace App\Exercise\Application\Dto;

use App\Exercise\Domain\Entity\Skill;

final class SkillWithAmountDto
{
    public function __construct(
        private readonly Skill $skill,
        private readonly int $amount,
    ) {
    }

    public function getSkill(): Skill
    {
        return $this->skill;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

}