<?php

declare(strict_types=1);

namespace App\Exercise\Application\Dto;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use OpenApi\Attributes as OA;

#[OA\Schema(
    schema: 'ExerciseCreateDto',
    title: 'Объект запроса для создания/редактирования задания',
    properties: [
        new OA\Property(
            property: 'title',
            type: 'string',
            example: 'exercise 4'
        ),
        new OA\Property(
            property: 'description',
            type: 'string',
            example: 'some description'
        ),
    ]
)]

final class ExerciseCreateDto
{
    public function __construct(
        #[Assert\NotBlank(message: 'Название урока обязательно')]
        private readonly string $title,
        private readonly string $description,
        /** @param SkillIdWithAmountDto[] $skills */
        #[Assert\Type(type: 'array')]
        #[Serializer\Type(name: 'array<App\Exercise\Application\Dto\SkillIdWithAmountDto>')]
        private readonly array $skills,
    )
    {
    }

    public function getTitle(): string
    {
        return $this->title;

    }

    public function getDescription(): string
    {
        return $this->description;
    }

    /** @return  SkillIdWithAmountDto[]*/
    public function getSkills(): array
    {
        return $this->skills;
    }

}