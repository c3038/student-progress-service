<?php

declare(strict_types=1);

namespace App\Exercise\Application\Dto;

use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Attributes as OA;

#[OA\Schema(
    schema: 'SkillCreateDto',
    title: 'Объект запроса для создания/редактирования навыка',
    properties: [
        new OA\Property(
            property: 'title',
            type: 'string',
            example: 'skill 1'
        ),
        new OA\Property(
            property: 'description',
            type: 'string',
            example: 'some description'
        ),
    ]
)]
final class SkillCreateDto
{
    public function __construct(
        #[Assert\NotBlank(message: 'Название навыка обязательно')]
        private readonly string $title,
        private readonly string $description,
    ) {
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

}