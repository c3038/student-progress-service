<?php

declare(strict_types=1);

namespace App\Exercise\Application\Dto;

final class SWADto
{
    public function __construct(
        private readonly string $title,
        private readonly string $description,
        private readonly int    $amount
    )
    {
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}