<?php

declare(strict_types=1);

namespace App\Exercise\Application\Dto;

use OpenApi\Attributes as OA;

#[OA\Schema(
    schema: 'SkillIdWithAmountDto',
    title: 'Объект запроса для связи навыков с весом',
    properties: [
        new OA\Property(
            property: 'id',
            type: 'string',
            example: '01HFNSPQV1MJ8N5KCWF625E8Q4'
        ),
        new OA\Property(
            property: 'amount',
            type: 'integer',
            example: 12
        ),
    ]
)]
final class SkillIdWithAmountDto
{
    public function __construct(
        private readonly string $id,
        private readonly int $amount,
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

}