<?php

declare(strict_types=1);

namespace App\Exercise\Application\Dto;

final class ExerciseSkillsCollectionDto
{
    /** @param ExerciseSkillCreateDto[] $data */
    public function __construct(private readonly array $data)
    {
    }

    /** @return ExerciseSkillCreateDto[] */
    public function getData(): array
    {
        return $this->data;
    }
}