<?php

declare(strict_types=1);

namespace App\Exercise\Application\Dto;

final class ExerciseSkillCreateDto
{
    public function __construct(
        private readonly string $exerciseId,
        private readonly string $skillId,
        private readonly int    $amount,
    )
    {
    }

    public function getExerciseId(): string
    {
        return $this->exerciseId;
    }

    public function getSkillId(): string
    {
        return $this->skillId;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }
}