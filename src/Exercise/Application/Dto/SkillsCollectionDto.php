<?php

declare(strict_types=1);

namespace App\Exercise\Application\Dto;

final class SkillsCollectionDto
{
    /** @param SkillCreateDto[] $data */
    public function __construct(private readonly array $data)
    {
    }

    /** @return SkillCreateDto[] */
    public function getData(): array
    {
        return $this->data;
    }
}