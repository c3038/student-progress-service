<?php

declare(strict_types=1);

namespace App\Exercise\Application\Manager;

use App\Exercise\Application\Dto\SkillCreatedDto;
use App\Exercise\Application\Dto\SkillCreateDto;
use App\Exercise\Application\Dto\SkillsCollectionDto;
use App\Exercise\Application\Mapper\SkillMapper;
use App\Exercise\Domain\Entity\Skill;
use App\Exercise\Domain\Factory\SkillFactory;
use App\Exercise\Infrastructure\Repository\SkillRepository;
use App\Shared\Domain\Exception\EntityNotFoundException;

class SkillManager
{
    public function __construct(
        private readonly SkillRepository $skillRepository,
        private readonly SkillFactory    $skillFactory,
    )
    {
    }

    public function create(SkillCreateDto $dto): SkillCreatedDto
    {
        $skill = $this->skillFactory->create(
            $dto->getTitle(), $dto->getDescription()
        );
        $this->skillRepository->add($skill);

        return (new SkillMapper())->toCreatedDto($skill);
    }

    /**
     * @throws EntityNotFoundException
     */
    public function update(SkillCreateDto $dto, string $skillId): SkillCreatedDto
    {
        $skill = $this->getSkillById($skillId);

        $this->skillRepository->add(
            $skill
                ->setTitle($dto->getTitle())
                ->setDescription($dto->getDescription())
        );

        return (new SkillMapper())->toCreatedDto($skill);
    }

    /**
     * @throws EntityNotFoundException
     */
    public function findSkillById(string $skillId): SkillCreatedDto
    {
        return (new SkillMapper())->toCreatedDto($this->getSkillById($skillId));
    }

    public function findAll(): SkillsCollectionDto
    {
        $skills = $this->skillRepository->findAll();

        return new SkillsCollectionDto(array_map(
            static fn($skill) => (new SkillMapper())->toCreatedDto($skill),
            $skills
        ));
    }

    /**
     * @throws EntityNotFoundException
     */
    public function destroy(string $skillId): bool
    {
        $this->skillRepository->deleteSkill($this->getSkillById($skillId));

        return true;
    }

    private function getSkillById(string $skillId): Skill
    {
        return $this->skillRepository->findByUlid($skillId)
            ?? throw new EntityNotFoundException(sprintf('Не удалось найти навык по идентификатору = %s', $skillId));
    }

}