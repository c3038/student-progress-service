<?php

declare(strict_types=1);

namespace App\Exercise\Application\Manager;

use App\Exercise\Application\Dto\ExerciseCollectionDto;
use App\Exercise\Application\Dto\ExerciseCreateDto;
use App\Exercise\Application\Dto\ExerciseDto;
use App\Exercise\Application\Mapper\ExerciseMapper;
use App\Exercise\Domain\Entity\Exercise;
use App\Exercise\Domain\Factory\ExerciseFactory;
use App\Exercise\Infrastructure\Repository\ExerciseRepository;
use App\Exercise\Infrastructure\Repository\ExerciseSkillRepository;
use App\Exercise\Infrastructure\Repository\SkillRepository;
use App\Shared\Domain\Exception\EntityNotFoundException;

class ExerciseManager
{
    public function __construct(
        private readonly ExerciseRepository      $exerciseRepository,
        private readonly ExerciseFactory         $exerciseFactory,
        private readonly SkillRepository         $skillRepository,
        private readonly ExerciseSkillRepository $exerciseSkillRepository,
    )
    {
    }

    public function create(ExerciseCreateDto $dto): ExerciseDto
    {
        $skillsWithAmount = $this->skillRepository->getSkillsByIdsWithAmount($dto->getSkills());
        $exercise = $this->exerciseFactory->create(
            $dto->getTitle(),
            $skillsWithAmount,
            $dto->getDescription()
        );

        $this->exerciseRepository->add($exercise);

        return (new ExerciseMapper())->toCreatedDto($exercise);
    }

    /**
     * @throws EntityNotFoundException
     */
    public function update(ExerciseCreateDto $dto, string $exerciseId)
    {
        $skillsWithAmount = $this->skillRepository->getSkillsByIdsWithAmount($dto->getSkills());

        $exercise = $this->getExerciseById($exerciseId);
        $this->clearExerciseSkillsByExercise($exercise);

        $exercise = $this->exerciseFactory->update(
            $exercise,
            $dto->getTitle(),
            $skillsWithAmount,
            $dto->getDescription()
        );

        $this->exerciseRepository->flush();

        return (new ExerciseMapper())->toCreatedDto($exercise);
    }

    /**
     * @throws EntityNotFoundException
     */
    public function findExerciseById(string $exerciseId): ExerciseDto
    {
        return (new ExerciseMapper())->toCreatedDto($this->getExerciseById($exerciseId));
    }

    public function findAll(): ExerciseCollectionDto
    {
        $exercise = $this->exerciseRepository->findAll();

        return new ExerciseCollectionDto(array_map(
            static fn($exercise) => (new ExerciseMapper())->toCreatedDto($exercise),
            $exercise
        ));
    }

    /**
     * @throws EntityNotFoundException
     */
    public function destroy(string $exerciseId): bool
    {
        $exercise = $this->getExerciseById($exerciseId);
        $this->clearExerciseSkillsByExercise($exercise);
        $this->exerciseRepository->deleteExercise($exercise);

        return true;
    }

    private function clearExerciseSkillsByExercise(Exercise $exercise): void
    {
        foreach ($exercise->getSkills() as $exerciseSkill) {
            $this->exerciseSkillRepository->removeWithoutFlush($exerciseSkill);
        }
    }

    private function getExerciseById(string $exerciseId): Exercise
    {
        return $this->exerciseRepository->findByUlid($exerciseId)
            ?? throw new EntityNotFoundException(sprintf('Не удалось найти задание по идентификатору = %s', $exerciseId));
    }
}