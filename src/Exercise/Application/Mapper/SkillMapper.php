<?php

declare(strict_types=1);

namespace App\Exercise\Application\Mapper;

use App\Exercise\Application\Dto\SkillCreatedDto;
use App\Exercise\Domain\Entity\Skill;

class SkillMapper
{
    public function toCreatedDto(Skill $skill): SkillCreatedDto
    {
        return new SkillCreatedDto(
            $skill->getId(),
            $skill->getTitle(),
            $skill->getDescription(),
            $skill->getCreatedAt()->format('d-m-Y H:i:s'),
            $skill->getUpdatedAt()->format('d-m-Y H:i:s'),
        );
    }
}