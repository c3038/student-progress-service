<?php

declare(strict_types=1);

namespace App\Exercise\Application\Mapper;

use App\Exercise\Application\Dto\ExerciseDto;
use App\Exercise\Domain\Entity\Exercise;

class ExerciseMapper
{
    public function toCreatedDto(Exercise $exercise): ExerciseDto
    {
        return new ExerciseDto(
            $exercise->getId(),
            $exercise->getTitle(),
            $exercise->getDescription(),
            $exercise->getSkills(),
            $exercise->getCreatedAt()->format('d-m-Y H:i:s'),
            $exercise->getUpdatedAt()->format('d-m-Y H:i:s'),
        );
    }
}