<?php

declare(strict_types=1);

namespace App\Exercise\Domain\Factory;

use App\Exercise\Domain\Entity\Skill;

class SkillFactory

{
    public function create(string $title, string $description = ''): Skill
    {
        return (new Skill($title))
            ->setDescription($description);
    }
}