<?php

declare(strict_types=1);

namespace App\Exercise\Domain\Factory;

use App\Exercise\Domain\Entity\Exercise;

class ExerciseFactory
{
    public function __construct(
        private readonly ExerciseSkillFactory $exerciseSkillFactory,
    )
    {
    }

    public function create(string $title, array $skillsWithAmount = [], string $description = ''): Exercise
    {
        $exercise = (new Exercise($title))
            ->setDescription($description);

        return $this->setExerciseSkillsToExercise($exercise, $skillsWithAmount);
    }

    public function update(
        Exercise $exercise,
        string $title,
        array $skillsWithAmount = [],
        string $description = ''
    ): Exercise
    {
        $exercise
            ->setTitle($title)
            ->setDescription($description);

        return $this->setExerciseSkillsToExercise($exercise, $skillsWithAmount);
    }

    public function setExerciseSkillsToExercise(Exercise $exercise, array $skillsWithAmount = []): Exercise
    {
        $exerciseSkills = array_map(
            fn($skill) => $this->exerciseSkillFactory->create(
                $exercise, $skill->getSkill(), $skill->getAmount()
            ),
            $skillsWithAmount
        );

        foreach ($exerciseSkills as $exerciseSkill) {
            $exercise->setSkills($exerciseSkill);
        }

        return  $exercise;
    }
}