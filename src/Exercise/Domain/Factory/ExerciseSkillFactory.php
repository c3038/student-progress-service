<?php

declare(strict_types=1);

namespace App\Exercise\Domain\Factory;

use App\Exercise\Domain\Entity\Exercise;
use App\Exercise\Domain\Entity\ExerciseSkill;
use App\Exercise\Domain\Entity\Skill;

class ExerciseSkillFactory

{
    public function create(Exercise $exercise, Skill $skill, int $amount): ExerciseSkill
    {
        return (new ExerciseSkill())
            ->setExercise($exercise)
            ->setSkill($skill)
            ->setAmount($amount);
    }
}