<?php

declare(strict_types=1);

namespace App\Exercise\Domain\Entity;

use App\Exercise\Infrastructure\Repository\ExerciseSkillRepository;
use App\Shared\Domain\Manager\UlidManger;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Types\UlidType;

#[ORM\Entity(repositoryClass: ExerciseSkillRepository::class)]
#[ORM\Table(name: 'exercise_skill')]
#[ORM\Index(columns: ['exercise_id'], name: 'exercise_skill__exercise_id__ind')]
#[ORM\Index(columns: ['skill_id'], name: 'exercise_skill__skill_id__ind')]
class ExerciseSkill
{
    #[ORM\Id]
    #[ORM\Column(name: 'id', type: UlidType::NAME, length: 26, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.ulid_generator')]
    private string $id;

    #[ORM\ManyToOne(targetEntity: 'Exercise', inversedBy: 'skills')]
    #[ORM\JoinColumn(name: 'exercise_id', referencedColumnName: 'id')]
    private Exercise $exercise;

    #[ORM\ManyToOne(targetEntity: 'Skill', inversedBy: 'exercises')]
    #[ORM\JoinColumn(name: 'skill_id', referencedColumnName: 'id')]
    private Skill $skill;

    #[ORM\Column(type: 'integer', options: ['unsigned'])]
    private int $amount;

    #[ORM\Column(name: 'created_at', type: 'datetime', nullable: false)]
    #[Gedmo\Timestampable(on: 'create')]
    #[JMS\Type("DateTime<'d-m-Y H:i:s'>")]
    private DateTime $createdAt;

    #[ORM\Column(name: 'updated_at', type: 'datetime', nullable: false)]
    #[Gedmo\Timestampable(on: 'update')]
    #[JMS\Type("DateTime<'d-m-Y H:i:s'>")]
    private DateTime $updatedAt;

    public function __construct()
    {
        $this->id = UlidManger::generate();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Exercise
     */
    public function getExercise(): Exercise
    {
        return $this->exercise;
    }

    public function setExercise(Exercise $exercise): self
    {
        $this->exercise = $exercise;
        return $this;
    }

    public function getSkill(): Skill
    {
        return $this->skill;
    }

    public function setSkill(Skill $skill): self
    {
        $this->skill = $skill;
        return $this;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;
        return $this;
    }



    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(): self
    {
        $this->createdAt = new DateTime();

        return $this;
    }

    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(): self
    {
        $this->updatedAt = new DateTime();

        return $this;
    }
}