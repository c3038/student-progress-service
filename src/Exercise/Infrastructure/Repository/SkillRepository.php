<?php

declare(strict_types=1);

namespace App\Exercise\Infrastructure\Repository;

use App\Exercise\Application\Dto\SkillIdWithAmountDto;
use App\Exercise\Application\Dto\SkillWithAmountDto;
use App\Exercise\Domain\Entity\Skill;
use App\Exercise\Domain\Repository\SkillRepositoryInterface;
use App\Shared\Domain\Manager\UlidManger;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\Query\Parameter;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Types\UlidType;
use Symfony\Component\Uid\AbstractUid;
use Symfony\Component\Uid\Ulid;

class SkillRepository extends ServiceEntityRepository implements SkillRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Skill::class);
    }

    public function add(Skill $skill): void
    {
        $this->_em->persist($skill);
        $this->_em->flush();
    }

    public function findByUlid(string $ulid): ?Skill
    {
        return $this->find($ulid);
    }

    public function deleteSkill(Skill $skill): void
    {
        $this->_em->remove($skill);
        $this->_em->flush();
    }

    /**
     * @param array<SkillIdWithAmountDto> $skillsIdsWithAmount
     * @return array<SkillWithAmountDto>|[]
     */
    public function getSkillsByIdsWithAmount(array $skillsIdsWithAmount): array
    {
        $ulidIdis = array_map(
            static fn($item) =>
                UlidManger::toHexadecimalUlid($item->getId()),
            $skillsIdsWithAmount
        );

        $skills = $this->findBy(['id' => $ulidIdis]);

        if(!count($skills) == count($skillsIdsWithAmount)){
            return [];
        }

        $newArray = [];
        foreach ($skillsIdsWithAmount as $item) {
            $newArray[$item->getId()] = $item->getAmount();
        }

        return array_map(
            static fn($skill) => new SkillWithAmountDto(
                $skill, $newArray[$skill->getId()]
            ),
            $skills
        );
    }
}