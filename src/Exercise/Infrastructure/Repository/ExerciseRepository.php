<?php

namespace App\Exercise\Infrastructure\Repository;

use App\Exercise\Domain\Entity\Exercise;
use App\Exercise\Domain\Repository\ExerciseRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ExerciseRepository extends ServiceEntityRepository implements ExerciseRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Exercise::class);
    }

    public function add(Exercise $exercise): void
    {
        $this->_em->persist($exercise);
        $this->_em->flush();
    }

    public function flush(): void
    {
        $this->_em->flush();
    }

    public function findByUlid(string $ulid): ?Exercise
    {
        return $this->find($ulid);
    }

    public function deleteExercise(Exercise $exercise): void
    {
        $this->_em->remove($exercise);
        $this->_em->flush();
    }
    public function removeWithoutFlush(Exercise $exercise): void
    {
        $this->_em->remove($exercise);
    }
}