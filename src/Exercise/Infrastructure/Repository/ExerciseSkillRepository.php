<?php

namespace App\Exercise\Infrastructure\Repository;

use App\Exercise\Domain\Entity\Exercise;
use App\Exercise\Domain\Entity\ExerciseSkill;
use App\Exercise\Domain\Repository\ExerciseRepositoryInterface;
use App\Exercise\Domain\Repository\ExerciseSkillRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ExerciseSkillRepository extends ServiceEntityRepository implements ExerciseSkillRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExerciseSkill::class);
    }

    public function add(ExerciseSkill $exercise): void
    {
        $this->_em->persist($exercise);
        $this->_em->flush();
    }

    public function preSave(ExerciseSkill $exercise): void
    {
        $this->_em->persist($exercise);
    }

    public function save(ExerciseSkill $exercise): void
    {
        $this->_em->flush();
    }

    public function findByUlid(string $ulid): ExerciseSkill
    {
        return $this->find($ulid);
    }

    public function removeWithoutFlush(ExerciseSkill $exercise): void
    {
        $this->_em->remove($exercise);
    }
}