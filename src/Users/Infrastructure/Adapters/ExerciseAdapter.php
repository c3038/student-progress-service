<?php

namespace App\Users\Infrastructure\Adapters;

use App\ModuleX\Infrastructure\API\Api;

class ExerciseAdapter
{
    public function __construct(private readonly Api $moduleXApi)
    {
    }

    public function getSomeData(): array
    {
        $data = $this->moduleXApi->getSomeData();
        // mapping
        $dataAfterMapping = $data;
        return $dataAfterMapping;
    }
}