<?php

declare(strict_types=1);

namespace App\Users\Application\Manager;

use App\Users\Application\Dto\UserCreatedDto;
use App\Users\Application\Dto\UserCreateDto;
use App\Users\Application\Mapper\UserMapper;
use App\Users\Domain\Factory\UserFactory;
use App\Users\Infrastructure\Repository\UserRepository;

class UserManager
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly UserFactory $userFactory,
    ) {
    }
    public function create(UserCreateDto $dto): UserCreatedDto
    {
        $user = $this->userFactory->create($dto->getEmail(), $dto->getPassword());
        $this->userRepository->add($user);

        return (new UserMapper())->toCreatedDto($user);
    }
}