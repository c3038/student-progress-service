<?php

declare(strict_types=1);

namespace App\Users\Application\Mapper;

use App\Users\Application\Dto\UserCreatedDto;
use App\Users\Domain\Entity\User;

class UserMapper
{
    public function toCreatedDto(User $user): UserCreatedDto
    {
        return new UserCreatedDto(
            $user->getId(),
            $user->getEmail(),
            $user->getCreatedAt()->format('d-m-Y'),
            $user->getUpdatedAt()->format('d-m-Y'),
        );
    }
}