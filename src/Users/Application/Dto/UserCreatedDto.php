<?php

declare(strict_types=1);

namespace App\Users\Application\Dto;

use OpenApi\Attributes as OA;

#[OA\Schema(
    schema: 'UserCreatedDto',
    title: 'Объект ответа для создания/редактирования пользователя',
    properties: [
        new OA\Property(
            property: 'id',
            type: 'string',
            example: '01HFNSPQV1MJ8N5KCWF625E8Q4'
        ),
        new OA\Property(
            property: 'email',
            type: 'string',
            example: 'email@mail.ru'
        ),
        new OA\Property(
            property: 'created_at',
            type: 'string',
            example: '23-11-2023 07:38:05'
        ),
        new OA\Property(
            property: 'updated_at',
            type: 'string',
            example: '23-11-2023 07:47:48'
        ),
    ]
)]
final class UserCreatedDto
{
    public function __construct(
        private readonly string $id,
        private readonly string $email,
        private readonly string $createdAt,
        private readonly string $updatedAt,
    )
    {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

}