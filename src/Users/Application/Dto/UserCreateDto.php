<?php

declare(strict_types=1);

namespace App\Users\Application\Dto;

use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Attributes as OA;

#[OA\Schema(
    schema: 'UserCreateDto',
    title: 'Объект запроса для создания/редактирования пользователя',
    properties: [
        new OA\Property(
            property: 'email',
            type: 'string',
            example: 'email@mail.ru'
        ),
        new OA\Property(
            property: 'password',
            type: 'string',
            example: 'Qwej12309!'
        ),
    ]
)]
final class UserCreateDto
{
    public function __construct(
        #[Assert\NotBlank(message: 'Пароль и email обязательны')]
        #[Assert\Email(message: 'email {{ value }} не валиден',)]
        private readonly string $email,
        #[Assert\NotBlank(message: 'Пароль и email обязательны')]
        #[Assert\PasswordStrength(message: 'Ваш пароль сильно легкий')]
        private readonly string $password,
    )
    {
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

}